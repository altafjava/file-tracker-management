<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 ">
			<div class="panel panel-primary">
				<div class="panel-heading">Reset Password</div>
				<div class="panel-body">
					<form:form method="post" modelAttribute="resetPasswordModel">
						<%-- <form:hidden path="" /> --%>
						<fieldset class="form-group">
							<form:label path="username">User Name</form:label>
							<form:select path="username" items="${resetPasswordModel.usernames}" class="form-control" required="required" />
							<form:errors path="username" cssClass="text-warning" />
						</fieldset>
						<fieldset class="form-group">
							<form:label path="newPassword">New Password</form:label>
							<form:input path="newPassword" type="password" class="form-control" required="required" />
							<form:errors path="newPassword" cssClass="text-warning" />
						</fieldset>
						<fieldset class="form-group">
							<form:label path="confirmPassword">Confirm Password</form:label>
							<form:input path="confirmPassword" type="password" class="form-control" required="required" />
							<form:errors path="confirmPassword" cssClass="text-warning" />
						</fieldset>
						<button type="submit" class="btn btn-primary">Reset Password</button>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="common/footer.jspf"%>
<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Homepage</div>
		<div class="panel-body">
			Welcome ${name}!! <a href="/file-entries">Click here</a> to manage your File Tracker.
		</div>
	</div>
</div>
<%@ include file="common/footer.jspf"%>
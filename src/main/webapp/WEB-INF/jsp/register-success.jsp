<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Success New User Registration</div>
		<div class="panel-body">
			New User <b>${username}</b> created Successfully!
		</div>
	</div>
</div>
<%@ include file="common/footer.jspf"%>
<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Success Reset Password</div>
		<div class="panel-body">
			${username} Password Reseted Successfully!
		</div>
	</div>
</div>
<%@ include file="common/footer.jspf"%>
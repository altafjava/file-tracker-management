<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 ">
			<div class="panel panel-primary">
				<div class="panel-heading">Add File Entry</div>
				<div class="panel-body">
					<form:form method="post" modelAttribute="fileEntry">
						<%-- <form:hidden path="id" /> --%>
						<%-- <fieldset class="form-group">
							<form:label path="fileNo">File No</form:label>
							<form:input path="fileNo" type="text" class="form-control" required="required" />
							<form:errors path="fileNo" cssClass="text-warning" />
						</fieldset> --%>
						<fieldset class="form-group">
							<form:label path="subject">Subject</form:label>
							<form:input path="subject" type="text" class="form-control" required="required" />
							<form:errors path="subject" cssClass="text-warning" />
						</fieldset>
						<fieldset class="form-group">
							<form:label path="fileOpeningDate">File Opening Date</form:label>
							<form:input path="fileOpeningDate" type="text" class="form-control" required="required" />
							<form:errors path="fileOpeningDate" cssClass="text-warning" />
						</fieldset>
						<fieldset class="form-group">
							<form:label path="sendTo">Send To</form:label>
							<form:select path="sendTo" items="${fileEntry.usernames}" class="form-control" required="required" />
							<form:errors path="sendTo" cssClass="text-warning" />
						</fieldset>
						<button type="submit" class="btn btn-primary">Save</button>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="common/footer.jspf"%>
<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<div>
			<a type="button" class="btn btn-primary btn-md" href="/add-entry">Add File Entry</a>
		</div>
	</sec:authorize>
	<br>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3>List of File Entries</h3>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>File No</th>
						<th>Subject</th>
						<th>File Opening Date</th>
						<th>Send To</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${fileEntries}" var="fileEntry">
						<tr>
							<td>${fileEntry.fileNo}</td>
							<td>${fileEntry.subject}</td>
							<td><fmt:formatDate value="${fileEntry.fileOpeningDate}" pattern="dd/MM/yyyy" /></td>
							<td>${fileEntry.sendTo}</td>
							<td>
								<a type="button" class="btn btn-primary" href="/update-entry?id=${fileEntry.fileNo}">Update</a>
								<a type="button" class="btn btn-success" href="/track?fileNo=${fileEntry.fileNo}">Track</a>
								<sec:authorize access="hasRole('ROLE_ADMIN')">
									<a type="button" class="btn btn-warning" href="/delete-entry?id=${fileEntry.fileNo}">Delete</a>
								</sec:authorize>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

</div>
<%@ include file="common/footer.jspf"%>
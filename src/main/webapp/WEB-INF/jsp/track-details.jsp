<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 ">
			<div class="panel panel-primary">
				<div class="panel-heading">Track Details</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-4">File No</div>
						<div class="col-sm-8">${trackDetail.fileNo}</div>
					</div>
					<div class="row">
						<div class="col-sm-4">Source</div>
						<div class="col-sm-8">${trackDetail.source}</div>
					</div>
					<div class="row">
						<div class="col-sm-4">Current</div>
						<div class="col-sm-8">${trackDetail.current}</div>
					</div>
					<div class="row">
						<div class="col-sm-4">History</div>
						<div class="col-sm-8">${trackDetail.history}</div>
					</div>
					<div class="row">
						<div class="col-sm-4">Created Date</div>
						<div class="col-sm-8">${trackDetail.createdDate}</div>
					</div>
					<div class="row">
						<div class="col-sm-4">Updated Date</div>
						<div class="col-sm-8">${trackDetail.updatedDate}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="common/footer.jspf"%>
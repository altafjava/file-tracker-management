package com.akhtar.ftm.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.akhtar.ftm.model.ResetPasswordModel;

@Component
public class ResetPasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {
		return ResetPasswordModel.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ResetPasswordModel resetPasswordModel = (ResetPasswordModel) o;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty");

		if (resetPasswordModel.getNewPassword().length() < 5 || resetPasswordModel.getNewPassword().length() > 20) {
			errors.rejectValue("newPassword", "Size.userForm.password");
		}
		if (!resetPasswordModel.getConfirmPassword().equals(resetPasswordModel.getNewPassword())) {
			errors.rejectValue("confirmPassword", "Diff.userForm.passwordConfirm");
		}
	}
}

package com.akhtar.ftm.controller;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.akhtar.ftm.entity.TrackDetail;
import com.akhtar.ftm.service.TrackDetailService;

@Controller
public class FileTrackController {

	@Autowired
	private TrackDetailService trackDetailService;

	@GetMapping("/track")
	public String track(@RequestParam long fileNo, ModelMap modelMap) {
		List<TrackDetail> trackDetails = trackDetailService.getTrackDetails(fileNo);
		TrackDetail trackDetail = null;
		if (trackDetails.isEmpty()) {
			trackDetail = new TrackDetail();
		} else {
			trackDetail = trackDetails.get(0);
			String history = trackDetails.stream().map(td -> td.getCurrent()).collect(Collectors.joining(" -> "));
			trackDetail.setHistory(history);
		}
		modelMap.put("trackDetail", trackDetail);
		return "track-details";
	}
}

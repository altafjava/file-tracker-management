package com.akhtar.ftm.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.akhtar.ftm.model.ResetPasswordModel;
import com.akhtar.ftm.model.UserModel;
import com.akhtar.ftm.service.FileEntryService;
import com.akhtar.ftm.service.SecurityService;
import com.akhtar.ftm.service.UserService;
import com.akhtar.ftm.validator.ResetPasswordValidator;
import com.akhtar.ftm.validator.UserValidator;

@Controller
public class UserAccountController {
	@Autowired
	private UserService userService;
	@Autowired
	private SecurityService securityService;
	@Autowired
	private UserValidator userValidator;
	@Autowired
	private FileEntryService fileEntryService;
	@Autowired
	private ResetPasswordValidator resetPasswordValidator;

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("userModel", new UserModel());
		return "registration";
	}

	@PostMapping("/registration")
	public String registration(@ModelAttribute("userModel") UserModel userModel, ModelMap modelMap, BindingResult bindingResult) {
		userValidator.validate(userModel, bindingResult);
		if (bindingResult.hasErrors()) {
			return "registration";
		}
		userService.save(userModel);
		String username = userModel.getUsername();
//		securityService.autoLogin(username, userModel.getPasswordConfirm());
		modelMap.put("username", username);
		return "register-success";
	}

	@GetMapping("/login")
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");
		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");
		return "login";
	}

	@RequestMapping(value = "/reset-password")
	public String addFileEntry(ModelMap modelMap, @Valid ResetPasswordModel resetPasswordModel, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
		if (httpServletRequest.getMethod().equals("GET")) {
			List<String> usernames = fileEntryService.getAllUsernames();
			Map<String, String> usernameMap = usernames.stream().collect(Collectors.toMap(username -> username, username -> username));
			resetPasswordModel.setUsernames(usernameMap);
			modelMap.put("resetPasswordModel", resetPasswordModel);
			return "reset-password";
		} else {
			resetPasswordValidator.validate(resetPasswordModel, bindingResult);
			if (bindingResult.hasErrors()) {
				List<String> usernames = fileEntryService.getAllUsernames();
				Map<String, String> usernameMap = usernames.stream().collect(Collectors.toMap(username -> username, username -> username));
				resetPasswordModel.setUsernames(usernameMap);
				return "reset-password";
			}
			String username = resetPasswordModel.getUsername();
			userService.resetPassword(username, resetPasswordModel.getNewPassword());
			modelMap.put("username", username);
			return "reset-success";
		}
	}
}

package com.akhtar.ftm.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.akhtar.ftm.entity.FileEntry;
import com.akhtar.ftm.model.FileEntryModel;
import com.akhtar.ftm.service.FileEntryService;
import com.akhtar.ftm.service.UserService;

@Controller
public class FileEntryController {

	@Autowired
	private FileEntryService entryService;
	@Autowired
	private UserService userService;

	@GetMapping("/file-entries")
	public String showAllFileEntries(ModelMap modelMap) {
		String name = userService.getLoggedInUserName(modelMap);
		System.err.println("LoggedInUserName=" + name);
		List<FileEntry> fileEntries = entryService.getAllFileEntries();
		modelMap.put("fileEntries", fileEntries);
		return "file-entries";
	}

	@RequestMapping(value = "/add-entry")
	public String addFileEntry(ModelMap modelMap, @Valid FileEntry fileEntry, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
		if (httpServletRequest.getMethod().equals("GET")) {
			FileEntryModel fileEntryModel = new FileEntryModel();
			List<String> usernames = entryService.getAllUsernames();
			Map<String, String> usernameMap = usernames.stream().collect(Collectors.toMap(username -> username, username -> username));
			fileEntryModel.setUsernames(usernameMap);
			modelMap.put("fileEntry", fileEntryModel);
			return "entry";
		} else {
			if (bindingResult.hasErrors()) {
				return "entry";
			}
			fileEntry.setUserName(userService.getLoggedInUserName(modelMap));
			entryService.saveFileEntry(fileEntry);
			return "redirect:/file-entries";
		}
	}

	@GetMapping("/update-entry")
	public String updateFileEntry(@RequestParam long id, ModelMap modelMap) {
		FileEntry fileEntry = entryService.getFileEntry(id);
		FileEntryModel fileEntryDto = new FileEntryModel();
		BeanUtils.copyProperties(fileEntry, fileEntryDto);
		List<String> usernames = entryService.getAllUsernames();
		Map<String, String> usernameMap = usernames.stream().collect(Collectors.toMap(username -> username, username -> username));
		fileEntryDto.setUsernames(usernameMap);
		modelMap.put("fileEntry", fileEntryDto);
		return "update-entry";
	}

	@PostMapping("/update-entry")
	public String updateFileEntry(ModelMap modelMap, @Valid FileEntryModel fileEntryDto, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
		if (bindingResult.hasErrors()) {
			return "update-entry";
		}
		fileEntryDto.setUserName(userService.getLoggedInUserName(modelMap));
		entryService.updateFileEntry(fileEntryDto);
		return "redirect:/file-entries";
	}

	@GetMapping("/delete-entry")
	public String deleteFileEntry(@RequestParam long id) {
		entryService.deleteFileEntry(id);
		return "redirect:/file-entries";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

}

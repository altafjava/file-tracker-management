package com.akhtar.ftm.service;

public interface SecurityService {

	String findLoggedInUsername();

	void autoLogin(String username, String password);
}

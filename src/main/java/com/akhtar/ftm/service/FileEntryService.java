package com.akhtar.ftm.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akhtar.ftm.entity.FileEntry;
import com.akhtar.ftm.entity.TrackDetail;
import com.akhtar.ftm.entity.User;
import com.akhtar.ftm.model.FileEntryModel;
import com.akhtar.ftm.repository.FileEntryRepository;
import com.akhtar.ftm.repository.TrackDetailRepository;
import com.akhtar.ftm.repository.UserRepository;

@Service
public class FileEntryService {

	@Autowired
	private FileEntryRepository fileEntryRepository;
	@Autowired
	private TrackDetailRepository trackDetailRepository;
	@Autowired
	private UserRepository userRepository;

	public void saveFileEntry(@Valid FileEntry fileEntry) {
		Date date = new Date();
		fileEntry.setModifiedBy(fileEntry.getUserName());
		fileEntry.setCreatedBy(fileEntry.getUserName());
		fileEntry.setCreatedDate(date);
		fileEntry.setUpdatedDate(date);
		fileEntry = fileEntryRepository.save(fileEntry);
		TrackDetail trackDetail = new TrackDetail();
		trackDetail.setCreatedDate(date);
		trackDetail.setUpdatedDate(date);
		trackDetail.setCurrent(fileEntry.getSendTo());
		trackDetail.setFileNo(fileEntry.getFileNo());
		trackDetail.setSource(fileEntry.getUserName());
		trackDetailRepository.save(trackDetail);
	}

	public List<FileEntry> getAllFileEntries() {
		return fileEntryRepository.findAll();
	}

	public FileEntry getFileEntry(long id) {
		return fileEntryRepository.findByFileNo(id);
	}

	public void updateFileEntry(FileEntryModel fileEntryDto) {
		Date date = new Date();
		FileEntry fileEntry = new FileEntry();
		BeanUtils.copyProperties(fileEntryDto, fileEntry);
		fileEntry.setModifiedBy(fileEntry.getUserName());
		fileEntry.setUpdatedDate(date);
		fileEntry = fileEntryRepository.save(fileEntry);
		TrackDetail trackDetail = new TrackDetail();
		trackDetail.setFileNo(fileEntry.getFileNo());
		trackDetail.setCreatedDate(date);
		trackDetail.setUpdatedDate(date);
		trackDetail.setSource(fileEntry.getUserName());
		trackDetail.setCurrent(fileEntry.getSendTo());
		trackDetailRepository.save(trackDetail);
	}

	public void deleteFileEntry(long id) {
		fileEntryRepository.deleteById(id);
	}

	public List<String> getAllUsernames() {
		List<User> users = userRepository.findAll();
		return users.stream().map(User::getUsername).collect(Collectors.toList());
	}
}

package com.akhtar.ftm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akhtar.ftm.entity.TrackDetail;
import com.akhtar.ftm.repository.TrackDetailRepository;

@Service
public class TrackDetailService {

	@Autowired
	private TrackDetailRepository trackDetailRepository;

	public List<TrackDetail> getTrackDetails(long fileNo) {
		return trackDetailRepository.findByFileNo(fileNo);
	}
}

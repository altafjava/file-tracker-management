package com.akhtar.ftm.service;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.akhtar.ftm.entity.Role;
import com.akhtar.ftm.entity.User;
import com.akhtar.ftm.repository.RoleRepository;
import com.akhtar.ftm.repository.UserRepository;

@Service
public class StartupService {

	@Value("${willAdminDataSave}")
	private boolean willAdminDataSave;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@PostConstruct
	public void startup() {
		if (willAdminDataSave) {
			try {
				List<Role> roles = new ArrayList<>();
				Role role = new Role();
				role.setId(1);
				role.setName("ROLE_ADMIN");
				roles.add(role);
				roleRepository.save(role);
				role = new Role();
				role.setId(2);
				role.setName("ROLE_USER");
				roles.add(role);
				roleRepository.save(role);
				User user = new User();
				user.setId(1);
				user.setUsername("admin");
				user.setPassword(bCryptPasswordEncoder.encode("admin"));
				user.setRoles(roles);
				userRepository.save(user);
				System.err.println("-------------- Default Username Password Saved -------------------");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

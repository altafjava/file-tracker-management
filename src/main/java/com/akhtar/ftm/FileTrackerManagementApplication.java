package com.akhtar.ftm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileTrackerManagementApplication {

	private static final Logger logger = LoggerFactory.getLogger(FileTrackerManagementApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(FileTrackerManagementApplication.class, args);
		logger.info("------------------- APPLICATION STARTED SUCCESSFULLY ---------------------------");
	}
}

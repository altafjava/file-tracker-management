package com.akhtar.ftm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akhtar.ftm.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByName(String name);
}

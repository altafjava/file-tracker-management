package com.akhtar.ftm.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akhtar.ftm.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}

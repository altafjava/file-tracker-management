package com.akhtar.ftm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akhtar.ftm.entity.TrackDetail;

public interface TrackDetailRepository extends JpaRepository<TrackDetail, Long> {

	List<TrackDetail> findByFileNo(long fileNo);
}

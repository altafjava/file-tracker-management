package com.akhtar.ftm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akhtar.ftm.entity.FileEntry;

@Repository
public interface FileEntryRepository extends JpaRepository<FileEntry, Long> {

	FileEntry findByFileNo(long id);
}

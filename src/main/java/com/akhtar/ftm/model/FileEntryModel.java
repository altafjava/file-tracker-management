package com.akhtar.ftm.model;

import java.util.Date;
import java.util.Map;

public class FileEntryModel {

	private long fileNo;
	private String userName;
	private String subject;
	private Date fileOpeningDate;
	private String sendTo;
	private Map<String, String> usernames;
	private String createdBy;
	private String modifiedBy;
	private Date createdDate;
	private Date updatedDate;

	public long getFileNo() {
		return fileNo;
	}

	public void setFileNo(long fileNo) {
		this.fileNo = fileNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getFileOpeningDate() {
		return fileOpeningDate;
	}

	public void setFileOpeningDate(Date fileOpeningDate) {
		this.fileOpeningDate = fileOpeningDate;
	}

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public Map<String, String> getUsernames() {
		return usernames;
	}

	public void setUsernames(Map<String, String> usernames) {
		this.usernames = usernames;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
